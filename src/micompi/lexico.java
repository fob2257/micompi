/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micompi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Saul
 */
class lexico {

    nodo cabeza = null, p;
    int estado = 0;
    int columna, valorMT;
    int numRenglon = 0;
    char caracter;
    String lexema = "";
    String archivo = "C:\\Users\\Saul\\Documents\\NetBeansProjects\\miCompi\\Codigo.txt";

    boolean errorEncontrado = false;

    int matriz[][] = {
              // l    d    +    -    *    %    /    &    |    !    =    .    ,    :    ;    (    )    [    ]    {    }    "    >    <    '    $    @   eb   rt   nl  tab  eof   oc
              // 0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29   30   31   32
        /*0*/{   1,   2, 103, 104, 105, 107,   5,   9,  10,  11,  12, 121, 118, 119, 120, 122, 123, 124, 125, 126, 127,  13,  14,  15,  16, 128, 129,   0,   0,   0,   0,   0, 506},
        /*1*/{   1,   1, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100},
        /*2*/{ 101,   2, 101, 101, 101, 101, 101, 101, 101, 101, 101,   3, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101, 101},
        /*3*/{ 500,   4, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500},
        /*4*/{ 102,   4, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102},
        /*5*/{ 106, 106, 106, 106,   6, 106,   8, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106, 106},
        /*6*/{   6,   6,   6,   6,   7,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6, 501,   6},
        /*7*/{   6,   6,   6,   6,   6,   6,   0,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6,   6},
        /*8*/{   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   8,   0,   0,   8,   8,   8},
        /*9*/{ 502, 502, 502, 502, 502, 502, 502, 114, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502, 502},
       /*10*/{ 503, 503, 503, 503, 503, 503, 503, 503, 115, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503, 503},
       /*11*/{ 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 112, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116, 116},
       /*12*/{ 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 113, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117, 117},
       /*13*/{  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13,  13, 130,  13,  13,  13,  13,  13,  13,  13, 504,  13,  13,  13},
       /*14*/{ 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 110, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108, 108},
       /*15*/{ 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 111, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109},
       /*16*/{  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16,  16, 131,  16,  16,  16,  16, 505,  16,  16,  16}
    };

    String palReservadas[][] = {
        //   l        d    
        //   1        2   
        /*0*/ {"new", "200"},
        /*1*/ {"if", "201"},// sentencia
        /*2*/ {"else", "202"},// sentencia
        /*3*/ {"for", "203"},// sentencia
        /*4*/ {"false", "204"},
        /*5*/ {"true", "205"},
        /*6*/ {"Float", "206"},
        /*7*/ {"main", "207"},
        /*8*/ {"print", "208"},
        /*9*/ {"bool", "209"},
        /*10*/ {"while", "210"} // sentencia
    };

    String Errores[][] = {
        //   l        d    
        //   1        2   
        /*0*/{"Se espera 'digito'", "500"},
        /*1*/ {"'eof' inesperado", "501"},
        /*2*/ {"Se espera un '&'", "502"},
        /*3*/ {"Se espera un '|'", "503"},
        /*4*/ {"Nueva linea inesperada", "504"},
        /*5*/ {"Se espera un '", "505"},
        /*6*/ {"Simbolo no valido", "506"}

    };

    public lexico() throws FileNotFoundException, IOException {
        FileReader file = new FileReader(archivo);
        BufferedReader buff = new BufferedReader(file);

        boolean eof = false;
        String linea = "";
        String linea2 = "";
        while (!eof && linea != null) {
//        while(caracter != -1){
//        linea = buff.readLine() + "\n";
            linea = buff.readLine();
            linea2=linea+"\r \n";
            numRenglon++;

            if (linea == null) {
                insertarNodo();
            } else {
                for (int i = 0; i < linea2.length(); i++) {
                    caracter = linea2.charAt(i);
                    if (Character.isLetter(caracter)) {
                        columna = 0;
                    } else if (Character.isDigit(caracter)) {
                        columna = 1;
                    } else {
                        switch (caracter) {
                            case '+':
                                columna = 2;
                                break;
                            case '-':
                                columna = 3;
                                break;
                            case '*':
                                columna = 4;
                                break;
                            case '%':
                                columna = 5;
                                break;
                            case '/':
                                columna = 6;
                                break;
                            case '&':
                                columna = 7;
                                break;
                            case '|':
                                columna = 8;
                                break;
                            case '!':
                                columna = 9;
                                break;
                            case '=':
                                columna = 10;
                                break;
                            case '.':
                                columna = 11;
                                break;
                            case ',':
                                columna = 12;
                                break;
                            case ':':
                                columna = 13;
                                break;
                            case ';':
                                columna = 14;
                                break;
                            case '(':
                                columna = 15;
                                break;
                            case ')':
                                columna = 16;
                                break;
                            case '[':
                                columna = 17;
                                break;
                            case ']':
                                columna = 18;
                                break;
                            case '{':
                                columna = 19;
                                break;
                            case '}':
                                columna = 20;
                                break;
                            case '"':
                                columna = 21;
                                break;
                            case '>':
                                columna = 22;
                                break;
                            case '<':
                                columna = 23;
                                break;
                            case '\'':
                                columna = 24;
                                break;
                            case '$':
                                columna = 25;
                                break;
                            case '@':
                                columna = 26;
                                break;
                            case ' ':
                                columna = 27;
                                break; //espacio en blanco
                            case 13:
                                columna = 28;
                                break;  // rt
                            case 10:
                                columna = 29;
                                break;  // nl
                            case 9:
                                columna = 30;
                                break;   // tab
                            case 3:
                                columna = 31;
                                break;   // eof
                            default:
                                columna = 32;
                                break;// oc
                        }
                    }
                    valorMT = matriz[estado][columna];
//                System.out.println("caracter leido: "+caracter);
                    if (valorMT < 100) {   //cambiar de estado
                        estado = valorMT;
//                lexema = lexema+caracter;

                        if (estado == 0) {
                            lexema = "";
                        } else {
                            lexema = lexema + caracter;
                        }
                    } else if (valorMT >= 100 && valorMT < 500) {

                        if (valorMT == 100) {
                            validarSiEsPalabraReservada();
                        }

                        if (valorMT == 100 || valorMT == 101 || valorMT == 102 || valorMT == 106 || valorMT == 116
                                || valorMT == 117 || valorMT == 108 || valorMT == 109) {
                            i--;
                        } else {
                            lexema = lexema + caracter;
                        }

//                System.out.println("Imprimir Nodo: "+lexema+" "+valorMT+" "+numRenglon);
                        insertarNodo();

                        estado = 0;
                        lexema = "";
                    } else {
//                System.out.println("Error: "+valorMT);
                        imprimirMensajeError();
                        break;
                    }

                }
                if (errorEncontrado) {
                    break;
                }
            }
        }

    }

    private void validarSiEsPalabraReservada() {
        for (int i = 0; i < palReservadas.length; i++) {
            if (lexema.equals(palReservadas[i][0])) {
                valorMT = Integer.valueOf(palReservadas[i][1]);
            }
        }
    }

    private void imprimirMensajeError() {
        for (int i = 0; i < Errores.length; i++) {
            if (valorMT == Integer.valueOf(Errores[i][1])) {
                String color = "\u001B[31m";
                System.out.println(color + "Error " + valorMT + " encontrado: " + Errores[i][0] + " en el renglon " + numRenglon);
                errorEncontrado = true;
            }
        }
    }

    private void insertarNodo() {
        nodo nodo = new nodo(lexema, valorMT, numRenglon);

        if (cabeza == null) {
            cabeza = nodo;
            p = cabeza;
        } else {
            p.sig = nodo;
            p = nodo;
        }
    }
}
