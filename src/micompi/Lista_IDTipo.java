/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micompi;

/**
 *
 * @author Saul
 */
class Lista_IDTipo {

    //Atributos de la lista
    nodo_IDTipo Pri;
    nodo_IDTipo Ult;
    String Nom;

    //Constructor de la lista
    public Lista_IDTipo(String n) {
        Pri = Ult = null;
        Nom = n;
    }

    //Constructor
    public boolean Lista_IDTipoVacia() {//EN CASO DE  QUE LA LISTA ESTE VACIA
        if (Pri == null) {
            return true;
        } else {
            return false;
        }
    }

    //Metodo para insertar por la parte posterior de la lista
    public void Insertar_Nodo_Final(String lexema, String tipo) {
        if (Lista_IDTipoVacia()) {
            Pri = Ult = new nodo_IDTipo(lexema, tipo);
        } else {
            Ult = Ult.sig = new nodo_IDTipo(lexema, tipo);
        }
    }

    public void Mostrar() {
        nodo_IDTipo Actual = Pri;
        if (Lista_IDTipoVacia()) {
            System.out.println("La " + Nom + " esta vacia");
        }
        
    System.out.println("--Variables--");
        while (Actual != null) {
            System.out.print(Actual.lexema);
            System.out.print("\t");
            System.out.println(Actual.tipo);
            Actual = Actual.sig;
        }
    }
//Metodo para buscar en la lista de inicion a fin

    public boolean nodoEncontrado(String xlexema) {
        boolean encontrado = false;
        if (!Lista_IDTipoVacia()) {
            nodo_IDTipo Actual = Pri;

            while (Actual != null) {

                if (Actual.lexema.equals(xlexema)) {

                    encontrado = true;
                    break;

                }
                Actual = Actual.sig;
            }

        }
        return encontrado;
    }

    public boolean tipoEncontrado(String tipo) {
        boolean encontrado = false;
        if (!Lista_IDTipoVacia()) {
            nodo_IDTipo Actual = Pri;

            while (Actual != null) {

                if (Actual.tipo.equals(tipo)) {
                    encontrado = true;
                    break;
                }
                Actual = Actual.sig;
            }

        }
        return encontrado;
    }

    public void cambiarTipo(String xlexema, String tipo) {
        if (!Lista_IDTipoVacia()) {
            nodo_IDTipo Actual = Pri;

            while (Actual != null) {

                if (Actual.lexema.equals(xlexema)) {
                    Actual.tipo = tipo;
                }
                Actual = Actual.sig;
            }

        }
    }

    public String sacarTipo(String xlexema) {
        if (!Lista_IDTipoVacia()) {
            nodo_IDTipo Actual = Pri;

            while (Actual != null) {

                if (Actual.lexema.equals(xlexema)) {
                    return Actual.tipo;

                }
                Actual = Actual.sig;
            }

        }

        return null;

    }

    public boolean encontrarVar(String xlexema, String tipo) {
        boolean encontrado = false;
        if (!Lista_IDTipoVacia()) {
            nodo_IDTipo Actual = Pri;

            while (Actual != null) {

                if (Actual.lexema.equals(xlexema) && Actual.tipo.equals(tipo)) {
                    encontrado = true;
                    break;
                }
                Actual = Actual.sig;
            }

        }
        return encontrado;
    }
}
