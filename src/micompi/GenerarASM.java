/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micompi;

//import com.sun.xml.internal.ws.util.StringUtils;
import java.util.Stack;
import java.io.*;

/**
 *
 * @author Saul
 */
class GenerarASM {

    private listaPolisher polisher;
    Stack variablesDeclaradas = new Stack();
    Stack variablesTemporales = new Stack();
    listaPolisher listaVariables =new listaPolisher();
    private int temporal = 3;

    GenerarASM(listaPolisher polisher) {
        this.polisher = polisher;
        genenerarDocumentoASM();
    }

    private void genenerarDocumentoASM() {
        ///CODIGO
        String codigoASM = obtenerCodigo();

        String parteArriba = ""
                + ";/StartHeader\n"
                + "INCLUDE macros.mac\n"
                + "DOSSEG\n"
                + ".MODEL SMALL\n"
                + "STACK 100h\n"
                + ".DATA\n"
                + "			BUFFER		DB 8 DUP('$')  ;23h\n"
                + "			BUFFERTEMP	DB 8 DUP('$')  ;23h\n"
                + "			BLANCO	DB '#'\n"
                + "			BLANCOS	DB '$'\n"
                + "			MENOS	DB '-$'\n"
                + "			COUNT	DW 0\n"
                + "			NEGATIVO	DB 0\n"
                + "			ARREGLO	DW 0\n"
                + "			ARREGLO1	DW 0\n"
                + "			ARREGLO2	DW 0\n"
                + "			LISTAPAR	LABEL BYTE\n"
                + "			LONGMAX	DB 254\n"
                + "			TOTCAR	DB ?\n"
                + "			INTRODUCIDOS	DB 254 DUP ('$')\n"
                + "			MULT10	DW 1\n"
                + "			cadena db 'Hola Mundo','$'\n"
                + stringvariablesTemporales()
                + stringVariablesDeclaradas()
                + "\n"
                + ".CODE\n"
                + ".386\n"
                + "BEGIN:\n"
                + "			MOV     AX, @DATA\n"
                + "			MOV     DS, AX\n"
                + "CALL  COMPI\n"
                + "			MOV AX, 4C00H\n"
                + "			INT 21H\n"
                + "COMPI  PROC";

        //Obtener plantilla bottom
        String parteabajo = ""
                + "		ret\n"
                + "COMPI  ENDP\n"
                + "END BEGIN";

        File f;
        f = new File("codigo" + ".asm");
        try {
            FileWriter w = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(w);
            PrintWriter wr = new PrintWriter(bw);
            wr.write(parteArriba);//escribimos en el archivo
            wr.write(";PAWN");
            String resultadocompilador = obtenerCodigo();
            wr.append(resultadocompilador);
            wr.append(System.lineSeparator());
            wr.append(parteabajo); //concatenamos
            wr.close();
            bw.close();
        } catch (IOException e) {
        }

    }

    private String obtenerCodigo() {
        StringBuilder codigoFinal = new StringBuilder();
        Stack pila = new Stack();

        nodo_IDTipo Actual = polisher.Pri;
        while (Actual != null) {
            if (Actual.tipo.equals("operador") || Actual.tipo.equals("brinco") || Actual.tipo.equals("puntero")) {
                if (Actual.tipo.equals("operador")) {
                    this.temporal++;
                    int temporallocal = temporal - 3;

                    nodo_IDTipo elemento2 = (nodo_IDTipo) pila.pop();
                    nodo_IDTipo elemento1 = (nodo_IDTipo) pila.pop();

                    if (!isInt(elemento1.lexema)) {
                        if (!elemento1.lexema.contains("T")) {
                            
                            this.variablesDeclaradas.push(elemento1);
                            
                            //listaVariables.encontrar()

                            

                        }

                    }
// this.variablesDeclaradas.push(elemento1);
                    if (Actual.lexema.equals("117")) {//  =
                        codigoFinal.append(System.lineSeparator());
                        codigoFinal.append("\t");
                        codigoFinal.append("\t");
                        codigoFinal.append("MOV  ax," + elemento2.lexema);
                        codigoFinal.append(System.lineSeparator());
                        codigoFinal.append("\t");
                        codigoFinal.append("\t");
                        codigoFinal.append("MOV " + elemento1.lexema + ",ax");
                    } else {
                        if (Actual.lexema.equals("103")) {// +
                            codigoFinal.append(System.lineSeparator());
                            codigoFinal.append("\t");
                            codigoFinal.append("\t");
                            nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                            pila.push(temporalpila);
                            codigoFinal.append("SUMAR " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                        } else {
                            if (Actual.lexema.equals("104")) { //-
                                codigoFinal.append(System.lineSeparator());
                                codigoFinal.append("\t");
                                codigoFinal.append("\t");

                                nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                pila.push(temporalpila);
                                codigoFinal.append("RESTA " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                            } else {
                                if (Actual.lexema.equals("105")) { //*
                                    codigoFinal.append(System.lineSeparator());
                                    codigoFinal.append("\t");
                                    codigoFinal.append("\t");

                                    nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                    pila.push(temporalpila);
                                    codigoFinal.append("MULTI " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                } else {
                                    if (Actual.lexema.equals("106")) { // division
                                        codigoFinal.append(System.lineSeparator());
                                        codigoFinal.append("\t");
                                        codigoFinal.append("\t");

                                        nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                        pila.push(temporalpila);
                                        codigoFinal.append("DIVIDE " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                    } else {
                                        if (Actual.lexema.equals("108")) { // >
                                            codigoFinal.append(System.lineSeparator());
                                            codigoFinal.append("\t");
                                            codigoFinal.append("\t");

                                            nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                            pila.push(temporalpila);
                                            codigoFinal.append("I_MAYOR " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                        } else {
                                            if (Actual.lexema.equals("109")) { // <
                                                codigoFinal.append(System.lineSeparator());
                                                codigoFinal.append("\t");
                                                codigoFinal.append("\t");

                                                nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                                pila.push(temporalpila);
                                                codigoFinal.append("I_MENOR " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                            } else {
                                                if (Actual.lexema.equals("110")) { // >=
                                                    codigoFinal.append(System.lineSeparator());
                                                    codigoFinal.append("\t");
                                                    codigoFinal.append("\t");

                                                    nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                                    pila.push(temporalpila);
                                                    codigoFinal.append("I_MAYORIGUAL " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                                } else {
                                                    if (Actual.lexema.equals("111")) { // <=
                                                        codigoFinal.append(System.lineSeparator());
                                                        codigoFinal.append("\t");
                                                        codigoFinal.append("\t");

                                                        nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                                        pila.push(temporalpila);
                                                        codigoFinal.append("I_MENORIGUAL " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                                    } else {
                                                        if (Actual.lexema.equals("112")) { // <=
                                                            codigoFinal.append(System.lineSeparator());
                                                            codigoFinal.append("\t");
                                                            codigoFinal.append("\t");

                                                            nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                                            pila.push(temporalpila);
                                                            codigoFinal.append("I_DIFERENTES " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                                        } else {
                                                            if (Actual.lexema.equals("113")) { // ==
                                                                codigoFinal.append(System.lineSeparator());
                                                                codigoFinal.append("\t");
                                                                codigoFinal.append("\t");

                                                                nodo_IDTipo temporalpila = new nodo_IDTipo("T" + temporallocal, "operando");
                                                                pila.push(temporalpila);
                                                                codigoFinal.append("I_IGUAL " + elemento1.lexema + "," + elemento2.lexema + "," + "T" + temporallocal);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                } else if (Actual.tipo.equals("puntero")) {
                    codigoFinal.append(System.lineSeparator());
                    codigoFinal.append("\t");
                    codigoFinal.append("\t");
                    codigoFinal.append(Actual.lexema + ":"); // P1:
                } else if (Actual.tipo.equals("brinco")) {
                    if (Actual.lexema.equals("BRF")) {
                        codigoFinal.append(System.lineSeparator());
                        codigoFinal.append("\t");
                        codigoFinal.append("\t");
                        codigoFinal.append("JF " + "T" + (this.temporal - 3) + "," + Actual.valor);
                    } else if (Actual.lexema.equals("BRI")) {
                        codigoFinal.append(System.lineSeparator());
                        codigoFinal.append("\t");
                        codigoFinal.append("\t");
                        codigoFinal.append("JMP " + Actual.valor);
                    }

                }

            } else {// si es un operando 
                pila.push(Actual);
            }
            Actual = Actual.sig;
        }

        return codigoFinal.toString();
    }

    private String stringVariablesDeclaradas() {
        StringBuilder resultado = new StringBuilder();
        while (!variablesDeclaradas.empty()) {
            nodo_IDTipo elemento = (nodo_IDTipo) variablesDeclaradas.pop();
            resultado.append(System.lineSeparator());
            resultado.append("\t");
            resultado.append("\t");
            resultado.append("\t");
            resultado.append(elemento.lexema + " dw ?");

        }

        return resultado.toString();
    }

    private String stringvariablesTemporales() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        StringBuilder wr = new StringBuilder();
        for (int i = 1; i <= temporal + 10; i++) {
            wr.append(System.lineSeparator());
            wr.append("\t");
            wr.append("\t");
            wr.append("\t");
            wr.append("T" + i + " dw ?");
        }
        return wr.toString();
    }

    private boolean isInt(String s) {

        return s.matches("[-+]?\\d*\\.?\\d+");

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean buscarelemento(Stack pila, String lexema) {
        boolean aux = false;
        Stack nuevaPila = pila;
        while (!nuevaPila.empty()) {
            if (lexema.equals(((nodo_IDTipo) nuevaPila.pop()).lexema)) {
                aux = true;
            }

        }

        return aux;
    }
}
