package micompi;

import java.util.Stack.*;
import java.util.LinkedList.*;
import java.util.Stack;
import java.util.*;

class sintaxis {

    String tmpNombrePrograma = "";

    String Errores[][] = {
        //   l                       d    
        //   1                       2   
        /*0*/{"Se espera un digito", "500"},
        /*1*/ {"'eof' inesperado", "501"},
        /*2*/ {"Se espera un '&'", "502"},
        /*3*/ {"Se espera un '|'", "503"},
        /*4*/ {"Nueva linea inesperada", "504"},
        /*5*/ {"Se espera un '", "505"},
        /*6*/ {"Simbolo no valido", "506"},
        /*7*/ {"Se espera la palabra main", "507"},
        /*8*/ {"Se espera un id", "508"},
        /*9*/ {"Se espera un valor", "509"},
        /*10*/ {"Se espera un (", "510"},
        /*11*/ {"Se espera un )", "511"},
        /*12*/ {"Se espera un {", "512"},
        /*13*/ {"Error al crear una sentencia", "513"},
        /*14*/ {"Se espera un }", "514"},
        /*15*/ {"Error al crear la expresion", "515"},
        /*16*/ {"Se espera un valor", "516"},
        {"Variable ya declarada", "1000"},
        {"Incompatibilidad de tipos", "1002"}

    };
    listaTipos ListaParaVerificarOperacionesTipos = new listaTipos();
    private int puntero_P=0;
    private int puntero_Q=0;

    private void imprimirMensajeError(int numError) {
        for (int i = 0; i < Errores.length; i++) {
            if (numError == Integer.valueOf(Errores[i][1])) {
                System.out.println("Error: " + Errores[i][0] + " renglon: " + p.renglon);

            }
        }
    }

    nodo p;
    Lista_IDTipo lista_tipo = new Lista_IDTipo("lista tipo");
    String tipoVariable;

    Stack pila = new Stack();
    listaPolisher polisher = new listaPolisher();

    sintaxis(nodo cabeza) {

        p = cabeza;

//        while (p != null) {
        //--------------------------------
        if (p.token == 207) {//main 
            tmpNombrePrograma = p.lexema;
            p = p.sig;

            if (p.token == 122) {// (
                p = p.sig;

                if (p.token == 123) {// )
                    p = p.sig;
                    if (p.token == 126) { // {
                        p = p.sig;
                        if (identificarSentencia()) {
                            if (p.token == 127) { // }
                                polisher.Mostrar();
                                GenerarASM generador =new GenerarASM(polisher);

                            } else {
                                imprimirMensajeError(514);
                            }

                        } /*else {
                         imprimirMensajeError(513);
                         }*/

                    } else {
                        imprimirMensajeError(512);
                    }

                } else {
                    imprimirMensajeError(511);
                }
            } else {
                imprimirMensajeError(510);
            }
        } else {
            imprimirMensajeError(507);
        }
//        lista_tipo.Mostrar();
//        polisher.Mostrar();
        //pila.push("TOPE");
        //mostrarPila();

//        }
    }

    private boolean identificarSentencia() {
        if (p.token == 200) { // new
            nodo auxiliar2 = p.sig;
            if (auxiliar2.token == 209) {
                if (SentenciaBool()) {
                    if (p.token == 200 || p.token == 201 || p.token == 210 || p.token == 203 || p.token == 209) {
                        if (identificarSentencia()) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            }
            if (SentenciaNew()) {
                if (p.token == 200 || p.token == 201 || p.token == 210 || p.token == 203 || p.token == 209) {
                    if (identificarSentencia()) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }

        if (p.token == 201) { // if
            if (SentenciaIF()) { //metodo
                if (p.token == 200 || p.token == 201 || p.token == 210 || p.token == 203 || p.token == 209) {
                    if (identificarSentencia()) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        if (p.token == 210) { // while
            if (SentenciaWhile()) {
                if (p.token == 200 || p.token == 201 || p.token == 210 || p.token == 203 || p.token == 209) {
                    if (identificarSentencia()) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        if (p.token == 203) { // for
            if (SentenciaFor()) {
                if (p.token == 200 || p.token == 201 || p.token == 210 || p.token == 203 || p.token == 209) {
                    if (identificarSentencia()) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean SentenciaIF() {
        this.puntero_P++;
        int puntero_local_P=this.puntero_P;
        this.puntero_Q++;
        int puntero_local_Q=this.puntero_Q;
        if (p.token == 201) { // if
            p = p.sig;
            if (p.token == 122) {// (
                p = p.sig;
                identificarExpresion();
                
                //if (identificarExpresion()) {// bloque
                if (p.token == 123) {// )
                    p = p.sig;
                    if (p.token == 126) {// {
                        p = p.sig;
                        //BRF  P   P++
                        polisher.Insertar_alFinal("BRF", "brinco", ("P"+puntero_local_P));
                        //S1
                        identificarSentencia(); // bloque
                        //BRI  Q   Q++
                        polisher.Insertar_alFinal("BRI", "brinco", ("Q"+puntero_local_Q));
                        // P   puntero
                                    polisher.Insertar_alFinal(("P"+puntero_local_P),"puntero");
//                            p = p.sig;
                        if (p.token == 127) {// }
                            p = p.sig;

                            if (p.token == 202) {// else
                                p = p.sig;
                                if (p.token == 126) {// {
                                    p = p.sig;
                                    
                                    //S2
                                    
                                    identificarSentencia();// bloque
                                    // Q   puntero
                                     //polisher.Insertar_alFinal(("Q"+puntero_local_Q),"puntero");
                                    if (p.token == 127) { // }
                                        p = p.sig;
                                    // Q   puntero
                                    polisher.Insertar_alFinal(("Q"+puntero_local_Q),"puntero");
                                        return true;
                                    } else {
                                        imprimirMensajeError(514);
                                    }
                                } else {
                                    imprimirMensajeError(512);
                                }
                            } else {
                                // Q   puntero
                                     polisher.Insertar_alFinal(("Q"+puntero_local_Q),"puntero");
                                return true;
                            }
                        } else {
                            imprimirMensajeError(514);
                        }
                    } else {
                        imprimirMensajeError(512);
                    }
                } else {

                }
                //}
            } else {
                imprimirMensajeError(510);
            }
        }

        return false;
    }

    private boolean SentenciaWhile() {
        if (p.token == 210) {// while
            p = p.sig;
            if (p.token == 122) {// (
                p = p.sig;
                if (identificarExpresion()) {
                    if (p.token == 123) {// )
                        p = p.sig;
                        if (p.token == 126) {// {
                            p = p.sig;
                            if (identificarSentencia()) {
//                            p = p.sig;
                                if (p.token == 127) {// }
                                    p = p.sig;
                                    return true;
                                } else {
                                    imprimirMensajeError(514);
                                }
                            } else {

                            }
                        } else {
                            imprimirMensajeError(512);
                        }
                    } else {
                        imprimirMensajeError(511);
                    }
                } else {
                    imprimirMensajeError(515);
                }
            } else {
                imprimirMensajeError(510);
            }
        }
        return false;
    }

    private boolean SentenciaFor() {
        if (p.token == 203) {// for
            p = p.sig;
            if (p.token == 122) {// (
                p = p.sig;
                if (valorEntrada_For()) {// 
                    if (p.token == 120) {// ;
                        limpiarPila();
                        p = p.sig;
                        if (condicionTerminacion_For()) {
                            if (p.token == 120) {// ;
                                limpiarPila();
                                p = p.sig;
                                if (iteracionCiclo_For()) {
                                    if (p.token == 120) {// ;
                                        limpiarPila();
                                        p = p.sig;
                                        if (p.token == 123) {// )
                                            p = p.sig;
                                            if (p.token == 126) {// {
                                                p = p.sig;
                                                if (identificarSentencia()) {
                                                    if (p.token == 127) {// }
                                                        p = p.sig;
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private void insertar_Postfijo(int tokenPolish, String tipoPolish) {
       
        if (pila.empty()) {
            pila.push(tokenPolish);
            
        } else {
            
           
            String s = "1";

            int auxToken = (int) pila.peek();

            //cuando recibimos un * o / 
            if ((tokenPolish == 105) || (tokenPolish == 106)) { // *  /
                if (auxToken == 117) { // =
                    pila.push(tokenPolish);

                }
                if (auxToken == 103 || auxToken == 104) { // +  -
                    pila.push(tokenPolish);
                }
                if (auxToken == 105 || auxToken == 106) {// * /
                    if (auxToken == 105) {
                        polisher.Insertar_alFinal("105", "operador");
                        pila.pop();
                        pila.push(tokenPolish);
                    }
                    if (auxToken == 106) {
                        polisher.Insertar_alFinal("106", "operador");
                        pila.pop();
                        pila.push(tokenPolish);
                    }

                }
            }
            //cuando recibimos un + o -
            if ((tokenPolish == 103) || (tokenPolish == 104)) {
                if (auxToken == 117) { // =
                    pila.push(tokenPolish);

                }
                if (auxToken == 105 || auxToken == 106) {// * /
                    if (auxToken == 105) {
                        polisher.Insertar_alFinal("105", "operador");
//                        pila.pop();
                        pila.push(tokenPolish);
                    }
                    if (auxToken == 106) {
                        polisher.Insertar_alFinal("106", "operador");
//                        pila.pop();
                        pila.push(tokenPolish);
                    }
                }
                if (auxToken == 103 || auxToken == 104) {// +  - /
                    if (auxToken == 103) {
                        polisher.Insertar_alFinal("103", "operador");
                        pila.pop();
                        pila.push(tokenPolish);
                    }
                    if (auxToken == 104) {
                        polisher.Insertar_alFinal("104", "operador");
                        pila.pop();
                        pila.push(tokenPolish);
                    }
                }

            }
            if ((tokenPolish == 108) || (tokenPolish == 109) || (tokenPolish == 110) || (tokenPolish == 111) // > < >= <=
                    || (tokenPolish == 112) || (tokenPolish == 113)) { // != ==

                if (auxToken == 108) { //>
                    polisher.Insertar_alFinal("108", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
                if (auxToken == 109) {//<
                    polisher.Insertar_alFinal("109", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
                if (auxToken == 110) {//>=
                    polisher.Insertar_alFinal("110", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
                if (auxToken == 111) {//<=
                    polisher.Insertar_alFinal("111", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
                if (auxToken == 112) {//!=
                    polisher.Insertar_alFinal("112", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
                if (auxToken == 113) {//==
                    polisher.Insertar_alFinal("113", "operador");
                    pila.pop();
                    pila.push(tokenPolish);
                }
            }
        }

    }

    private boolean SentenciaNew() {
        nodo aux1 = p;

        if (p.token == 200) {// new
            //Asignar la variable tipo
//            tipoVariable = "int " + p.token;
            p = p.sig;
            if (p.token == 100) {// id
                tipoVariable = "int";
                aux1 = p;
//                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                //Buscar lexema en nodoSem

                if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                    imprimirMensajeError(1000);
                    //lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                    if (lista_tipo.encontrarVar(aux1.lexema, tipoVariable)) {
//                        imprimirMensajeError(1000);
//                    }
                } else {
                    lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                }

                polisher.Insertar_alFinal(aux1.lexema, tipoVariable);

                p = p.sig;
                if (p.token == 117) {// =
                    insertar_Postfijo(p.token, "operador");

                    p = p.sig;
                    if (p.token == 100) {// id
                        if (lista_tipo.nodoEncontrado(p.lexema)) {
                            polisher.Insertar_alFinal(p.lexema, lista_tipo.sacarTipo(p.lexema));
                        } else {
                            System.out.println("Variable no declarada");
                        }

                        if (identificarOperacion()) {
//                            p=p.sig; //nuevo
                            if (p.token == 120) {// ;
                                limpiarPila();
                                p = p.sig;
                                return true;
                            }
                        } else {
//                            p=p.sig; //nuevo
                            if (p.token == 120) {// ;
                                limpiarPila();
                                p = p.sig;
                                return true;
                            }
                        }
                    } else {
                        if (p.token == 101) {// digito
//                            if (lista_tipo.nodoEncontrado(aux1.lexema)) {
//                                lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                            } else {
//                                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
//                            }

                            if (identificarOperacion()) {
//                                p=p.sig; //nuevo
                                if (p.token == 120) {// ;
                                    limpiarPila();
                                    p = p.sig;
                                    return true;
                                }
                            } else {
//                                p=p.sig; //nuevo
                                if (p.token == 120) {// ;
                                    limpiarPila();
                                    p = p.sig;
                                    return true;
                                }
                            }
                        } else {
                            if (p.token == 131) {// '...'
                                tipoVariable = "char";
                                if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                                    lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                                    if (lista_tipo.encontrarVar(aux1.lexema, tipoVariable)) {
//                                        imprimirMensajeError(1000);
//                                    }
                                } else {
                                    lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                                }
                                p = p.sig;
                                if (p.token == 120) {// ;  
                                    try {
                                        limpiarPila();
                                    } catch (Exception e) {

                                    }
                                    p = p.sig;
                                    return true;
                                } else {
                                    imprimirMensajeError(509);
                                }
                            } else {
                                if (p.token == 130) {// "..."
                                    tipoVariable = "String";
                                    if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                                        lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
                                    } else {
                                        lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                                    }
                                    p = p.sig;
                                    if (p.token == 120) {// ;
                                        try {
                                            limpiarPila();
                                        } catch (Exception e) {

                                        }

                                        p = p.sig;
                                        return true;
                                    } else {
                                        imprimirMensajeError(509);
                                    }
                                } else {
                                    if (p.token == 104) {// -
                                        insertar_Postfijo(p.token, "operador");
                                        p = p.sig;
                                        if (p.token == 101) {// digito
//                                            if (lista_tipo.nodoEncontrado(aux1.lexema)) {
//                                                lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                                            } else {
//                                                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
//                                            }
                                            p = p.sig;
                                            if (p.token == 120) {// ;
                                                limpiarPila();
                                                p = p.sig;
                                                return true;
                                            } else {
                                                imprimirMensajeError(509);
                                            }
                                        } else {

                                        }
                                    } else {
                                        imprimirMensajeError(516);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (p.token == 120) {// ;
                        //limpiarPila();
//                        if (lista_tipo.nodoEncontrado(aux1.lexema)) {
//                            lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                        } else {
//                            lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
//                        }
                        p = p.sig;
                        return true;
                    } else {
                        imprimirMensajeError(509);
                    }
                }
            } else {
                if (p.token == 206) {// Float
//                    tipoVariable = "float " + p.token;
                    p = p.sig;
                    if (p.token == 119) {// :
                        p = p.sig;
                        if (p.token == 100) {// id
                            tipoVariable = "float";
                            aux1 = p;
                            if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                                imprimirMensajeError(1000);
//                                lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
                            } else {
                                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                            }
                            //
                            polisher.Insertar_alFinal(p.lexema, tipoVariable);
                            //buscar lexema en nodoSem
                            p = p.sig;
                            if (p.token == 117) {// =
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                if (p.token == 102) {// digito decimal
                                    if (identificarOperacion()) {
//                                        p=p.sig; //nuevo
                                        if (p.token == 120) {// ;
                                            limpiarPila(); // nuevo
                                            p = p.sig;
                                            return true;
                                        }
                                    } else {
//                                        p=p.sig; //nuevo
                                        if (p.token == 120) {// ;
                                            limpiarPila();
                                            p = p.sig;
                                            return true;
                                        }
                                    }
                                } else {
                                    if (p.token == 104) {// -
                                        insertar_Postfijo(p.token, "operador");
                                        p = p.sig;
                                        if (p.token == 102) {// digito decimal
//                                            if (lista_tipo.nodoEncontrado(aux1.lexema)) {
//                                                lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
//                                            } else {
//                                                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
//                                            }
                                            p = p.sig;
                                            if (p.token == 120) {// ;
                                                limpiarPila();
                                                p = p.sig;
                                                return true;
                                            } else {
                                                imprimirMensajeError(509);
                                            }
                                        } else {
                                            imprimirMensajeError(516);
                                        }
                                    } else {
                                        imprimirMensajeError(516);
                                    }
                                }
                            } else {
                                if (p.token == 120) {// ;
//                                    limpiarPila();
                                    p = p.sig;
                                    return true;
                                } else {
                                    imprimirMensajeError(509);
                                }
                            }
                        } else {
                            imprimirMensajeError(508);
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean identificarOperacion() {
        if (p.token == 100) { // id

            p = p.sig;
            if (p.token == 105) {// *
                insertar_Postfijo(p.token, "operador");
                p = p.sig;
                if (p.token == 100) {// id
                    identificarOperacion();
                } else {
                    if (p.token == 101) {// entero
                        tipoVariable = "int";
                        identificarOperacion();
                    } else {
                        if (p.token == 102) {// real
                            tipoVariable = "float";
                            identificarOperacion();
                        }
                    }
                }
            } else {
                if (p.token == 106) {// /
                    p = p.sig;
                    if (p.token == 100) {// id
                        identificarOperacion();
                    } else {
                        if (p.token == 101) {// entero
                            tipoVariable = "int";
                            identificarOperacion();
                        } else {
                            if (p.token == 102) {// real
                                tipoVariable = "float";
                                identificarOperacion();
                            }
                        }
                    }
                } else {
                    if (p.token == 103) {// +
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        if (p.token == 100) {// id
                            identificarOperacion();
                        } else {
                            if (p.token == 101) {// entero
                                tipoVariable = "int";
                                identificarOperacion();
                            } else {
                                if (p.token == 102) {// real
                                    tipoVariable = "float";
                                    identificarOperacion();
                                }
                            }
                        }
                    } else {
                        if (p.token == 104) {// -
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            if (p.token == 100) {// id
                                identificarOperacion();
                            } else {
                                if (p.token == 101) {// entero
                                    tipoVariable = "int";
                                    identificarOperacion();
                                } else {
                                    if (p.token == 102) {// real
                                        tipoVariable = "float";
                                        identificarOperacion();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (p.token == 101) { // entero
                tipoVariable = "int";
                polisher.Insertar_alFinal(p.lexema, tipoVariable);
                p = p.sig;
                if (p.token == 105) {// *
                    insertar_Postfijo(p.token, "operador");
//                    System.out.println("*******");
                    p = p.sig;
                    if (p.token == 100) {// id
                        identificarOperacion();
                    } else {
                        if (p.token == 101) {// entero
                            tipoVariable = "int";
                            identificarOperacion();

                        } else {
                            if (p.token == 102) {// real
                                tipoVariable = "float";
                                identificarOperacion();
                            }
                        }
                    }
                } else {
                    if (p.token == 106) {// /
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        if (p.token == 100) {// id
                            identificarOperacion();
                        } else {
                            if (p.token == 101) {// entero
                                tipoVariable = "int";
                                identificarOperacion();
                            } else {
                                if (p.token == 102) {// real
                                    tipoVariable = "float";
                                    identificarOperacion();
                                }
                            }
                        }
                    } else {
                        if (p.token == 103) {// +
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            if (p.token == 100) {// id
                                identificarOperacion();
                            } else {
                                if (p.token == 101) {// entero
                                    tipoVariable = "int";
                                    identificarOperacion();
                                } else {
                                    if (p.token == 102) {// real
                                        tipoVariable = "float";
                                        identificarOperacion();
                                    }
                                }
                            }
                        } else {
                            if (p.token == 104) {// -
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                if (p.token == 100) {// id
                                    identificarOperacion();
                                } else {
                                    if (p.token == 101) {// entero
                                        tipoVariable = "int";
                                        identificarOperacion();

                                    } else {
                                        if (p.token == 102) {// real
                                            tipoVariable = "float";
                                            identificarOperacion();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (p.token == 102) {// real
                    tipoVariable = "float";
                    polisher.Insertar_alFinal(p.lexema, tipoVariable);
                    p = p.sig;
                    if (p.token == 105) {// *
                        insertar_Postfijo(p.token, "operador");
//                        System.out.println("779******");
                        p = p.sig;
                        if (p.token == 100) {// id
                            identificarOperacion();
                        } else {
                            if (p.token == 101) {// entero
                                tipoVariable = "int";
                                identificarOperacion();
                            } else {
                                if (p.token == 102) {// real
                                    tipoVariable = "float";
                                    identificarOperacion();
                                }
                            }
                        }
                    } else {
                        if (p.token == 106) {// /
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            if (p.token == 100) {// id
                                identificarOperacion();
                            } else {
                                if (p.token == 101) {// entero
                                    tipoVariable = "int";
                                    identificarOperacion();
                                } else {
                                    if (p.token == 102) {// real
                                        tipoVariable = "float";
                                        identificarOperacion();
                                    }
                                }
                            }
                        } else {
                            if (p.token == 103) {// +
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                if (p.token == 100) {// id
                                    identificarOperacion();
                                } else {
                                    if (p.token == 101) {// entero
                                        tipoVariable = "int";
                                        identificarOperacion();
                                    } else {
                                        if (p.token == 102) {// real
                                            tipoVariable = "float";
                                            identificarOperacion();
                                        }
                                    }
                                }
                            } else {
                                if (p.token == 104) {// -
                                    insertar_Postfijo(p.token, "operador");

                                    p = p.sig;
                                    if (p.token == 100) {// id
                                        identificarOperacion();
                                    } else {
                                        if (p.token == 101) {// entero
                                            tipoVariable = "int";
                                            identificarOperacion();
                                        } else {
                                            if (p.token == 102) {// real
                                                tipoVariable = "float";
                                                identificarOperacion();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean SentenciaBool() {
        nodo aux1 = p;
        if (p.token == 200) {// new
            p = p.sig;
            if (p.token == 209) {// bool
//                tipoVariable = "bool " + p.token;
                p = p.sig;
                if (p.token == 100) {// id
                    tipoVariable = "bool";
                    aux1 = p;
                    p = p.sig;
                    if (p.token == 117) {// =
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        if (p.token == 204) {// false
                            if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                                imprimirMensajeError(1000);
//                                lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
                            } else {
                                lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                            }
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                p = p.sig;
                                return true;
                            }
                        } else {
                            if (p.token == 205) {//true
                                if (lista_tipo.nodoEncontrado(aux1.lexema)) {
                                    imprimirMensajeError(1000);
//                                    lista_tipo.cambiarTipo(aux1.lexema, tipoVariable);
                                } else {
                                    lista_tipo.Insertar_Nodo_Final(aux1.lexema, tipoVariable);
                                }
                                p = p.sig;
                                if (p.token == 120) {// ;
                                    limpiarPila();
                                    p = p.sig;
                                    return true;
                                }
                            } else {
                                imprimirMensajeError(509);
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    private boolean valorEntrada_For() {
        if (p.token == 200) {// new
            p = p.sig;
            if (p.token == 100) {// id
                p = p.sig;
                if (p.token == 117) {// =
                    insertar_Postfijo(p.token, "operador");
                    p = p.sig;
                    if (p.token == 101) {// digito
                        p = p.sig;
                        if (p.token == 120) {// ;
                            limpiarPila();
                            return true;
                        }
                    } else {
                        if (p.token == 100) {// id
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                return true;
                            }
                        }
                    }
                }
            }
        } else {
            if (p.token == 100) {// id
                p = p.sig;
                if (p.token == 117) {// =
                    insertar_Postfijo(p.token, "operador");
                    p = p.sig;
                    if (p.token == 101) {// digito
                        p = p.sig;
                        if (p.token == 120) {// ;
                            limpiarPila();
                            return true;
                        }
                    } else {
                        if (p.token == 100) {// id
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean condicionTerminacion_For() {
        if (p.token == 100) {// id
            p = p.sig;
            if (p.token == 117) {// =
                insertar_Postfijo(p.token, "operador");
                p = p.sig;
                if (p.token == 100) {// id
                    p = p.sig;
                    if (p.token == 120) {// ;
                        limpiarPila();
                        return true;
                    }
                } else {
                    if (p.token == 101) {// digito
                        p = p.sig;
                        if (p.token == 120) {// ;
                            limpiarPila();
                            return true;
                        }
                    }
                }
            } else {
                if (p.token == 108) {// >
                    p = p.sig;
                    if (p.token == 100) {// id
                        p = p.sig;
                        if (p.token == 120) {// ;
                            limpiarPila();
                            return true;
                        }
                    } else {
                        if (p.token == 101) {// digito
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                return true;
                            }
                        }
                    }
                } else {
                    if (p.token == 109) {// <
                        p = p.sig;
                        if (p.token == 100) {// id
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                return true;
                            }
                        } else {
                            if (p.token == 101) {// digito
                                p = p.sig;
                                if (p.token == 120) {// ;
                                    limpiarPila();
                                    return true;
                                }
                            }
                        }
                    } else {
                        if (p.token == 110) {// >=
                            p = p.sig;
                            if (p.token == 100) {// id
                                p = p.sig;
                                if (p.token == 120) {// ;
                                    limpiarPila();
                                    return true;
                                }
                            } else {
                                if (p.token == 101) {// digito
                                    p = p.sig;
                                    if (p.token == 120) {// ;
                                        limpiarPila();
                                        return true;
                                    }
                                }
                            }
                        } else {
                            if (p.token == 111) {// <=
                                p = p.sig;
                                if (p.token == 100) {// id
                                    p = p.sig;
                                    if (p.token == 120) {// ;
                                        limpiarPila();
                                        return true;
                                    }
                                } else {
                                    if (p.token == 101) {// digito
                                        p = p.sig;
                                        if (p.token == 120) {// ;
                                            limpiarPila();
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean iteracionCiclo_For() {
        if (p.token == 100) {// id
            p = p.sig;
            if (p.token == 117) {// =
                insertar_Postfijo(p.token, "operador");
                p = p.sig;
                if (p.token == 100) {// id
                    p = p.sig;
                    if (p.token == 103) {// +
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        if (p.token == 101) {// digito
                            p = p.sig;
                            if (p.token == 120) {// ;
                                limpiarPila();
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean identificarExpresion() {
        if (p.token == 100) {// id
            /*****/
            if (lista_tipo.nodoEncontrado(p.lexema)) {
                polisher.Insertar_alFinal(p.lexema, lista_tipo.sacarTipo(p.lexema));
            } else {
                System.out.println("Variable no declarada");
            }
            /*****/
            p = p.sig;
            if (p.token == 113) {// ==
                insertar_Postfijo(p.token, "operador");
                p = p.sig;
                identificarExpresion();
            } else {
                if (p.token == 114) {// &&
                    insertar_Postfijo(p.token, "operador");
                    p = p.sig;
                    identificarExpresion();
                } else {
                    if (p.token == 115) {// ||
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        identificarExpresion();
                    } else {
                        if (p.token == 108) {// >
                            
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            identificarExpresion();
                        } else {
                            if (p.token == 109) {// <
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                identificarExpresion();
                            } else {
                                if (p.token == 110) {// >=
                                    insertar_Postfijo(p.token, "operador");
                                    p = p.sig;
                                    identificarExpresion();
                                } else {
                                    if (p.token == 111) {// <=
                                        insertar_Postfijo(p.token, "operador");
                                        p = p.sig;
                                        identificarExpresion();
                                    } else {
                                        if (p.token == 123) {// )
                                            //                                    p = p.sig;
                                           limpiarPila();//lo que hay en pila lo pasa a lista polisher
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (p.token == 101) {// digito
                polisher.Insertar_alFinal(p.lexema, tipoVariable);
                p = p.sig;
                if (p.token == 113) {// ==
                    insertar_Postfijo(p.token, "operador");
                    p = p.sig;
                    identificarExpresion();
                } else {
                    if (p.token == 114) {// &&
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        identificarExpresion();
                    } else {
                        if (p.token == 115) {// ||
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            identificarExpresion();
                        } else {
                            if (p.token == 108) {// >
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                identificarExpresion();
                            } else {
                                if (p.token == 109) {// <
                                    insertar_Postfijo(p.token, "operador");
                                    p = p.sig;
                                    identificarExpresion();
                                } else {
                                    if (p.token == 110) {// >=
                                        insertar_Postfijo(p.token, "operador");
                                        p = p.sig;
                                        identificarExpresion();
                                    } else {
                                        if (p.token == 111) {// <=
                                            insertar_Postfijo(p.token, "operador");
                                            p = p.sig;
                                            identificarExpresion();
                                        } else {
                                            if (p.token == 123) {// )
                                                //                                    p = p.sig;
                                                limpiarPila();
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (p.token == 102) {// decimal
                    polisher.Insertar_alFinal(p.lexema, "float");
                    p = p.sig;
                    if (p.token == 113) {// ==
                        insertar_Postfijo(p.token, "operador");
                        p = p.sig;
                        identificarExpresion();
                    } else {
                        if (p.token == 114) {// &&
                            insertar_Postfijo(p.token, "operador");
                            p = p.sig;
                            identificarExpresion();
                        } else {
                            if (p.token == 115) {// ||
                                insertar_Postfijo(p.token, "operador");
                                p = p.sig;
                                identificarExpresion();
                            } else {
                                if (p.token == 108) {// >
                                    insertar_Postfijo(p.token, "operador");
                                    p = p.sig;
                                    identificarExpresion();
                                } else {
                                    if (p.token == 109) {// <
                                        insertar_Postfijo(p.token, "operador");
                                        p = p.sig;
                                        identificarExpresion();
                                    } else {
                                        if (p.token == 110) {// >=
                                            insertar_Postfijo(p.token, "operador");
                                            p = p.sig;
                                            identificarExpresion();
                                        } else {
                                            if (p.token == 111) {// <=
                                                insertar_Postfijo(p.token, "operador");
                                                p = p.sig;
                                                identificarExpresion();
                                            } else {
                                                if (p.token == 123) {// )
                                                    //                                    p = p.sig;
                                                    limpiarPila();
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private void limpiarPila() {
        while (true) {
            String auxPila = pila.lastElement().toString();
            polisher.Insertar_alFinal(auxPila, "operador");
            pila.pop();
            if (pila.empty()) {
//                System.out.println("PILA VACIA");
                break;
            }

        }
        //mostrarPostFijo();
        verificarTipos();

    }

    private void mostrarPila() {
        if (pila.empty()) {
            System.out.println("Alto Pila vacia");
        }
        System.out.println(pila.toString());
    }

    private void verificarTipos() {
        listaPolisher polisher2 = polisher;
        Stack pilaVerificadora = new Stack();

        nodo_IDTipo Actual = polisher2.Pri;
        if (polisher2.Lista_IDTipoVacia()) {
            System.out.println("La lista esta vacia");
        }

        while (Actual != null) {

            if (Actual.tipo.equals("operador")) {
                String tipo2 = (String) pilaVerificadora.peek();
//                System.out.println((String) pilaVerificadora.peek());
                //comente el pop de abajo
                //pilaVerificadora.pop();
                String tipo1 = (String) pilaVerificadora.peek();
                pilaVerificadora.pop();
                String tipoOperador;
                String ActualOperador = Actual.lexema;
                tipoOperador = ActualOperador.toString();
                switch (tipoOperador) {
                    case "105":
                        tipoOperador = "*";
                        break;
                    case "106":
                        tipoOperador = "/";
                        break;
                    case "103":
                        tipoOperador = "+";
                        break;
                    case "104":
                        tipoOperador = "-";
                        break;
                    case "117":
                        tipoOperador = "=";
                        break;
                    /*****/
                    case "108":
                        tipoOperador = ">";
                        break;
                    case "109":
                        tipoOperador = "<";
                        break;
                    case "110":
                        tipoOperador = ">=";
                        break;
                    case "111":
                        tipoOperador = "<=";
                        break;
                    case "112":
                        tipoOperador = "!=";
                        break;
                    case "113":
                        tipoOperador = "==";
                        break;
                    /*****/
                }
                if (ListaParaVerificarOperacionesTipos.tipoEncontrado(tipo1, tipoOperador, tipo2)) {
                    pilaVerificadora.push(ListaParaVerificarOperacionesTipos.tipoResultante(tipo1, tipoOperador, tipo2));
                } else {
                    imprimirMensajeError(1002); //Incompatibilidad de tipos
                }

            } else {
                pilaVerificadora.push(Actual.tipo);
            }
            //System.out.println(Actual.tipo);
            Actual = Actual.sig;
        }
        //mostrarPostFijo();

    }

    private void mostrarPostFijo() {
        //polisher.Mostrar();
        //polisher.Pri = null;
    }

}
