;/StartHeader
INCLUDE macros.mac
DOSSEG
.MODEL SMALL
STACK 100h
.DATA
			BUFFER		DB 8 DUP('$')  ;23h
			BUFFERTEMP	DB 8 DUP('$')  ;23h
			BLANCO	DB '#'
			BLANCOS	DB '$'
			MENOS	DB '-$'
			COUNT	DW 0
			NEGATIVO	DB 0
			ARREGLO	DW 0
			ARREGLO1	DW 0
			ARREGLO2	DW 0
			LISTAPAR	LABEL BYTE
			LONGMAX	DB 254
			TOTCAR	DB ?
			INTRODUCIDOS	DB 254 DUP ('$')
			MULT10	DW 1
			cadena db 'Hola Mundo','$'

			T1 dw ?
			T2 dw ?
			T3 dw ?
			T4 dw ?
			T5 dw ?
			T6 dw ?
			T7 dw ?
			T8 dw ?
			T9 dw ?
			T10 dw ?
			T11 dw ?
			T12 dw ?
			T13 dw ?
			T14 dw ?
			T15 dw ?
			T16 dw ?
			T17 dw ?
			T18 dw ?
			T19 dw ?
			T20 dw ?
			T21 dw ?
			T22 dw ?
			T23 dw ?
			a dw ?
			z dw ?
			v dw ?
.CODE
.386
BEGIN:
			MOV     AX, @DATA
			MOV     DS, AX
CALL  COMPI
			MOV AX, 4C00H
			INT 21H
COMPI  PROC;PAWN
		MOV  ax,2
		MOV a,ax
		MULTI 1,2,T12
		DIVIDE T12,2,T13
		RESTA 3,T13,T14
		MOV  ax,T14
		MOV v,ax
		I_IGUAL a,5,T16
		JF T16,P1
		MOV  ax,5
		MOV z,ax
		JMP Q1
		P1:
		I_MAYORIGUAL a,1,T18
		JF T18,P2
		I_MENOR a,4,T19
		JF T19,P3
		MOV  ax,9
		MOV z,ax
		JMP Q3
		P3:
		Q3:
		JMP Q2
		P2:
		Q2:
		Q1:

impresionw z
		ret
COMPI  ENDP
END BEGIN